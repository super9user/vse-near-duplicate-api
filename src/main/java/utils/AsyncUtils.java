package utils;

import config.CBIRConstants;

/**
 * Created by tanmay on 3/16/18.
 */
public class AsyncUtils {

    private static AsyncUtils instance = null;
    private AsyncUtils() {}

    public static AsyncUtils getInstance() {
        if(instance == null) {
            instance = new AsyncUtils();
        }
        return instance;
    }

    private int getOptimumThreadCount() {
        int count;
        try {
            count = Runtime.getRuntime().availableProcessors();
            if(count <= 0) throw new Exception("Invalid count " + count);
        } catch (Exception e) {
            System.out.println(e);
            count = CBIRConstants.DEFAULT_PARALLEL_THREADS;
        }
        if( !AppUtils.getInstance().isProd() ) count = count - 2; // to adjust for dev and test environments
        return count;
    }

}
