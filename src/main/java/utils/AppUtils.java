package utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import config.Environment;
import java.util.Map;

/**
 * Created by tanmay on 6/26/17.
 */


public class AppUtils {

    private static AppUtils instance = null;
    private AppUtils() {}

    public static AppUtils getInstance() {
        if(instance == null) {
            instance = new AppUtils();
        }
        return instance;
    }

    public boolean isDev() {
        if(Environment.JAVA_ENV == "dev") return true;
        return false;
    }

    public boolean isTest() {
        if(Environment.JAVA_ENV == "test") return true;
        return false;
    }

    public boolean isProd() {
        if(Environment.JAVA_ENV == "prod") return true;
        return false;
    }

    public JsonObject mapAsJson(Map<String, String> map) {
        JsonObject json = new JsonObject();
        for(Map.Entry entry: map.entrySet()) {
            json.addProperty(entry.getKey().toString(), entry.getValue().toString());
        }
        return json;
    }

    public String[] jsonToStringArray(JsonArray jArray) {
        String[] sArray = new String[jArray.size()];
        for(int i = 0; i < jArray.size(); i++ ) {
            sArray[i] = jArray.get(i).getAsString();
        }
        return sArray;
    }

    public JsonObject getParams(JsonObject opts, JsonObject optionalParams) {
        for (Map.Entry<String,JsonElement> entry : optionalParams.entrySet()) {
            String key = entry.getKey();
            JsonElement value = entry.getValue();
            if( !opts.has(key) ) opts.add(key, value);
        }
        return opts;
    }

    public boolean verifyOptions(JsonObject params, JsonArray compulsoryParams) {
        for (JsonElement param: compulsoryParams) {
            if(!params.has(param.getAsString())) {
                return false;
            }
        }
        return true;
    }

}
