package utils;

import java.io.File;
import java.util.Collection;
import javax.activation.MimetypesFileTypeMap;

/**
 * Created by tanmay on 8/11/17.
 */
public class FileUtils {

    private static FileUtils instance = null;
    private FileUtils() {}

    public static FileUtils getInstance() {
        if(instance == null) {
            instance = new FileUtils();
        }
        return instance;
    }

    public Collection getFilesInDir(String dirPath) {
        File dir = new File(dirPath);
        return org.apache.commons.io.FileUtils.listFiles(dir, null, true);
    }

    public boolean isValidImageFile(File f) {
        String mimeType = new MimetypesFileTypeMap().getContentType(f);
        String type = mimeType.split("/")[0];
        return type.equals("image");
    }

}
