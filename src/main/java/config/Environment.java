package config;

/**
 * Created by tanmay on 6/26/17.
 */
public class Environment {
    // All fields in this file should be modifiable at runtime.
    // So, do NOT use final keyword.
    // These are sort-a global variables for the app. Use with caution.

    public static String JAVA_ENV = "dev"; // default value

}
