package config;


import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import utils.AppUtils;

/**
 * Created by tanmay on 8/19/17.
 */
public class DatabaseConstants {

    // restrict instantiation
    private DatabaseConstants() {}

    public static final String DEFAULT_REPLICATION_STRATEGY = "SimpleStrategy";
    public static final int DEFAULT_REPLICATION_FACTOR = 1;
    public static final String KEYSPACE_NAME = "vse"; // TODO - change according to dev, test, prod env

    public static JsonObject DATABASE_DETAILS() {
        AppUtils appUtils = AppUtils.getInstance();
        if(appUtils.isDev()) return DEV_DATABASE();
        return null;
    }

    private static JsonObject DEV_DATABASE() {
        JsonArray contactPoints = new JsonArray();
        contactPoints.add("127.0.0.1");
        // TODO - get credentials from a common VSE registry such that its not visible in code
        JsonObject credentials = new JsonObject();
        credentials.addProperty("uname", "tanmay");
        credentials.addProperty("pwd", "tanmay");

        JsonObject obj = new JsonObject();
        obj.add("contactPoints", contactPoints);
        obj.add("credentials", credentials);
        obj.addProperty("port", 9042);
        return obj;
    }

}
