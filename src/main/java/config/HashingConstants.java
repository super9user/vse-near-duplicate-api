package config;

/**
 * Created by tanmay on 3/7/18.
 */
public class HashingConstants {

    private HashingConstants() {} // restrict instantiation

    // the number of stages is also sometimes called the number of bands
    public static final int STAGES = 3;
    // Attention: to get relevant results, the number of elements per bucket
    // should be at least 100
    public static final int BUCKETS = 100; // TODO - should be dynamically calculated
    // seed for producing consistent results
    // TODO - how to assign a better seed?
    public static final long SEED = 123456;
    // signature threshold
    public static final double THRESHOLD = 0.5;
    public static final long LARGE_PRIME =  433494437;

}
