package config;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Created by tanmay on 9/16/17.
 */
public class CBIRConstants {

    // restrict instantiation
    private CBIRConstants() {}

    public static final int DEFAULT_PARALLEL_THREADS = 4;
    public static final String FCTH_TABLE_NAME = "fcth_images";
    public static final String BUCKET_TABLE_NAME = "images_by_bucket";


    public static JsonArray DEV_IMAGES_PATH() {
        JsonArray jsonArray = new JsonArray();

        JsonObject grp1 = new JsonObject();
        grp1.addProperty("imageGroup", "standard_jpegs");
        grp1.addProperty("path", "/Users/tanmay/ProjectReads/Image_Database/standard_jpegs");

        JsonObject grp2 = new JsonObject();
        grp2.addProperty("imageGroup", "shapes");
        grp2.addProperty("path", "/Users/tanmay/ProjectReads/Image_Database/shapes/MPEG7_CE-Shape-1_Part_B");

        JsonObject grp3 = new JsonObject();
        grp3.addProperty("imageGroup", "simplicity");
        grp3.addProperty("path", "/Users/tanmay/ProjectReads/Image_Database/simplicity/images");

//        JsonObject grp4 = new JsonObject();
//        grp4.addProperty("imageGroup", "places");
//        grp4.addProperty("path", "/Users/tanmay/ProjectReads/Image_Database/places2/val_256");

        JsonObject grp5 = new JsonObject();
        grp5.addProperty("imageGroup", "imagenet");
        grp5.addProperty("path", "/Users/tanmay/ProjectReads/Image_Database/imagenet");

        JsonObject grp6 = new JsonObject();
        grp6.addProperty("imageGroup", "corel");
        grp6.addProperty("path", "/Users/tanmay/ProjectReads/Image_Database/corel/images");

        jsonArray.add(grp1);
        jsonArray.add(grp2);
        jsonArray.add(grp3);
//        jsonArray.add(grp4);
        jsonArray.add(grp5);
        jsonArray.add(grp6);

        return jsonArray;
    }

}
