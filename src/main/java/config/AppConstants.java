package config;

/**
 * Created by tanmay on 6/21/17.
 */

public final class AppConstants {

    private AppConstants() {
        // restrict instantiation
    }

    public static final int PORT = 8081;
    public static final int ARGS_ENV = 0;

    public static final int OK_STATUS = 200;
    public static final int BAD_REQUEST_STATUS = 400;
    public static final int INTERNAL_SERVER_ERROR = 500;

    public static final String DISTANCE_STRATEGY_TANIMOTO = "tanimoto";
    public static final String DISTANCE_STRATEGY_MANHATTAN = "manhattan";
    public static final String DISTANCE_STRATEGY_JACCARD = "jaccard";

    public static final String AVERAGE_STRATEGY_MEAN = "mean";
    public static final String AVERAGE_STRATEGY_MEDIAN = "median";

    public static final String HASHING_STRATEGY_MINHASH = "minhash";
    public static final String HASHING_STRATEGY_SUPERBIT = "superbit";
}