package routes;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import spark.Request;
import spark.Response;
import utils.AppUtils;
import java.util.Map;
import static utils.JsonUtil.json;

/**
 * Created by tanmay on 7/11/17.
 */
public abstract class Route {

    protected String responseType = "application/json";
    protected AppUtils appUtils = AppUtils.getInstance();
    protected Gson gson = new Gson();
    protected abstract Object begin(Request req, Response res);

    protected void get(String routeName) {
        spark.Spark.get(routeName, (req, res) -> this.begin(req, res), json());
    }

    protected Object getResponse(Response res, ResponseTemplate responseTemplate) {
        res.type(responseType);
        if(responseTemplate.getStatus() != null) res.status(responseTemplate.getStatus());
        return responseTemplate.getData();
    }

    protected String getJsonParams(JsonObject params) {
        return gson.toJson(params);
    }

    protected JsonObject getParamsAsJson(Map<String, String> params) {
        return appUtils.mapAsJson(params);
    }

}
