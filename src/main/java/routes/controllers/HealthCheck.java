package routes.controllers;

import routes.Route;
import spark.Request;
import spark.Response;

/**
 * Created by tanmay on 2/3/18.
 */
public class HealthCheck extends Route {

    HealthCheck() {
        this.get("/healthcheck");
    }

    @Override
    protected Object begin(Request req, Response res) {
        return "Everything looks fine to me!";
    }
}
