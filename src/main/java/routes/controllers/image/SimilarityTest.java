package routes.controllers.image;

import com.google.gson.JsonObject;
import routes.Route;
import spark.Request;
import spark.Response;

/**
 * Created by tanmay on 3/10/18.
 */
public class SimilarityTest extends Route {

    SimilarityTest() {
        this.get("/image/similarity");
    }

    @Override
    protected Object begin(Request req, Response res) {
        middleware.SimilarityTest similarityTest = new middleware.SimilarityTest();
        JsonObject params = getParamsAsJson(req.params());
        params.addProperty("inputImagePath", "/Users/tanmay/ProjectReads/Image_Database/corel/images/CorelDB 3/art_antiques/435000.jpg");
        return getResponse(res, similarityTest.execute(params));
    }

}
