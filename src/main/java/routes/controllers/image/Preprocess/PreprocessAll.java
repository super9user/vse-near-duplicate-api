package routes.controllers.image.Preprocess;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import config.CBIRConstants;
import middleware.preprocessing.CreateAll;
import modules.GetFeatureVector;
import routes.Route;
import spark.Request;
import spark.Response;

/**
 * Created by tanmay on 2/3/18.
 */
public class PreprocessAll extends Route {

    public PreprocessAll() {
        this.get("/image/preprocess");
    }

    @Override
    protected Object begin(Request req, Response res) {
        CreateAll createAll = new CreateAll();
        JsonObject params = this.getParamsAsJson(req.params());
        params.add("imageDirs", this.getImageDirectories()); // TODO - cleanup
        return this.getResponse(res, createAll.execute(params));
    }

    private JsonArray getImageDirectories() {
        if(this.appUtils.isDev()) {
            return CBIRConstants.DEV_IMAGES_PATH();
        } else {
            return null;
        }
    }
}
