package routes.controllers.image.Preprocess;

import com.google.gson.JsonObject;
import middleware.preprocessing.CreateBuckets;
import routes.Route;
import spark.Request;
import spark.Response;

/**
 * Created by tanmay on 2/23/19.
 */
public class PreprocessBuckets extends Route {

    public PreprocessBuckets() {
        this.get("/image/preprocess/buckets");
    }

    @Override
    protected Object begin(Request req, Response res) {
        CreateBuckets createBuckets = new CreateBuckets();
        JsonObject params = this.getParamsAsJson(req.params());
        return this.getResponse(res, createBuckets.execute(params));
    }
}