package routes.controllers.image;

import routes.controllers.image.Preprocess.PreprocessAll;
import routes.controllers.image.Preprocess.PreprocessBuckets;

/**
 * Created by tanmay on 2/3/18.
 */
public class Index {
    public Index() {
        new PreprocessAll();
        new PreprocessBuckets();
        new Search();
        new SimilarityTest();
    }
}
