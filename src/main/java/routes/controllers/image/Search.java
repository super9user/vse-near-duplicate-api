package routes.controllers.image;

import com.google.gson.JsonObject;
import middleware.search.HashingSearcher;
import routes.Route;
import spark.Request;
import spark.Response;

/**
 * Created by tanmay on 2/3/18.
 */
public class Search extends Route {

    public Search() {
        this.get("/image/search");
    }

    @Override
    protected Object begin(Request req, Response res) {
        HashingSearcher searcher = new HashingSearcher();
        JsonObject params = getParamsAsJson(req.params());
        params.addProperty("inputImagePath", "/Users/tanmay/ProjectReads/Image_Database/corel/images/CorelDB 3/art_antiques/435000.jpg");
        return getResponse(res, searcher.execute(params));
    }

}
