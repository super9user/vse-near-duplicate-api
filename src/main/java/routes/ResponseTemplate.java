package routes;

/**
 * Created by tanmay on 7/11/17.
 */
public class ResponseTemplate {

    private Integer status = null;
    private Object data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
