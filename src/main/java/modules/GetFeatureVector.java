package modules;

import com.google.gson.*;
import modules.feature_vector.FCTHFeatureVector;
import java.io.File;

/**
 * Created by tanmay on 6/26/17.
 */

public class GetFeatureVector extends VseModule {

    Gson gson;
    FCTHFeatureVector fcthFeatureVector;
    JsonObject output;

    public GetFeatureVector() {
        gson = new Gson();
        fcthFeatureVector = new FCTHFeatureVector();
        output = new JsonObject();
    }

    @Override
    public JsonObject begin() throws Exception {
        File img = gson.fromJson(this.params.get("img"), File.class);
        double[] featureVector = fcthFeatureVector.getFeatureVector(img);
        output.add("featureVector", gson.toJsonTree(featureVector));
        return output;
    }

    @Override
    public void setCompulsoryParams() {
        this.compulsoryParams.add("img");
    }

    @Override
    public void setOptionalParamsWithDefaults() {}

}
