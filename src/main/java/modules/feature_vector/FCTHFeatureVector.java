package modules.feature_vector;

import net.semanticmetadata.lire.imageanalysis.features.global.FCTH;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;

/**
 * Created by tanmay on 9/6/17.
 */
public class FCTHFeatureVector {

    FCTH fcth = new FCTH();
    public double[] getFeatureVector(File img) throws Exception {
        FileInputStream fis = new FileInputStream(img);
        BufferedImage buffImg = ImageIO.read(fis);
        fcth.extract(buffImg);
        return fcth.getFeatureVector();
    }

}
