package modules;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import config.AppConstants;
import modules.hashing.HashingStrategy;
import modules.hashing.MinHashing;
import modules.hashing.SuperbitHashing;

/**
 * Created by tanmay on 3/15/18.
 */
public class GetHashingBuckets extends VseModule {

    Gson gson;
    JsonObject output;
    public GetHashingBuckets() {
        gson = new Gson();
        output = new JsonObject();
    }

    @Override
    protected JsonObject begin() throws Exception {
        double[] vector = gson.fromJson(this.params.get("vector"), double[].class);
        HashingStrategy hashingStrategy;

        switch (this.params.get("strategy").getAsString()) {
            case AppConstants.HASHING_STRATEGY_MINHASH:
                hashingStrategy = new MinHashing();
                break;

            case AppConstants.HASHING_STRATEGY_SUPERBIT:
                hashingStrategy = new SuperbitHashing();
                break;

            default:
                throw new Exception("Strategy not found: " + this.params.get("strategy").getAsString());
        }

        output.add("buckets", gson.toJsonTree(hashingStrategy.getBuckets(vector)));
        return output;
    }

    @Override
    protected void setCompulsoryParams() {
        this.compulsoryParams.add("vector");
    }

    @Override
    protected void setOptionalParamsWithDefaults() {
        this.optionalParams.addProperty("strategy", AppConstants.HASHING_STRATEGY_SUPERBIT);
    }
}
