package modules.distances.sort_distances;

import java.util.Comparator;

/**
 * Created by tanmay on 9/9/17.
 */
public class ImageDistanceComparator implements Comparator<ImageDistance> {
    @Override
    public int compare(ImageDistance o1, ImageDistance o2) {
        if( o1.getDistance() < o2.getDistance() )
            return -1;
        else if( o2.getDistance() < o1.getDistance() )
            return 1;
        return 0;
    }
}
