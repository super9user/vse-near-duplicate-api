package modules.distances.sort_distances;

/**
 * Created by tanmay on 9/9/17.
 */
public class ImageDistance {

    String imagePath;
    double distance;

    public ImageDistance() {}

    public ImageDistance(String imagePath, double distance) {
        this.imagePath = imagePath;
        this.distance = distance;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

}
