package modules.distances;

/**
 * Created by tanmay on 9/9/17.
 */
public class TanimotoDistance extends DistanceStrategy {

    @Override
    public double getDistance(double[] v1, double[] v2, double v1Sum, double v2Sum) throws Exception {
        if(v1.length != v2.length) {
            throw new Exception("Dimensions do not match");
        } else {
            double distResult;
            double distTmp1 = v1Sum;
            double distTmp2 = v2Sum;
            double distTmpCnt1 = 0.0D;
            double distTmpCnt2 = 0.0D;
            double distTmpCnt3 = 0.0D;

            if(distTmp1 == 0.0D && distTmp2 == 0.0D) {
                return 0.0D;
            } else if(distTmp1 != 0.0D && distTmp2 != 0.0D) {
                for(int i = 0; i < v1.length; ++i) {
                    distTmpCnt1 += v1[i] / distTmp1 * (v2[i] / distTmp2);
                    distTmpCnt2 += v2[i] / distTmp2 * (v2[i] / distTmp2);
                    distTmpCnt3 += v1[i] / distTmp1 * (v1[i] / distTmp1);
                }

                distResult = 100.0D - 100.0D * (distTmpCnt1 / (distTmpCnt2 + distTmpCnt3 - distTmpCnt1));
                return distResult;
            } else {
                return 100.0D;
            }
        }
    }

}
