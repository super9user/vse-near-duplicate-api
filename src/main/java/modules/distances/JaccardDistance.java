package modules.distances;

import com.google.common.primitives.Doubles;


import java.util.*;

/**
 * Created by tanmay on 3/4/18.
 */
public class JaccardDistance extends DistanceStrategy {

    @Override
    public double getDistance(double[] v1, double[] v2) throws Exception {
        List<Double> v1List = Doubles.asList(v1);
        List<Double> v2List = Doubles.asList(v2);
        Set<Double> a = new HashSet<>(v1List);
        Set<Double> b = new HashSet<>(v2List);

        Set<Double> union = new HashSet<>();
        union.addAll(a);
        union.addAll(b);

        int inter = a.size() + b.size() - union.size();

        return 1.0 - (1.0 * inter / union.size());
    }

}
