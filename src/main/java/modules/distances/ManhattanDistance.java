package modules.distances;

/**
 * Created by tanmay on 9/10/17.
 */
public class ManhattanDistance extends DistanceStrategy {
    @Override
    public double getDistance(double[] v1, double[] v2) throws Exception {
        double distance = 0;
        for ( int i = 0; i < v1.length; i++) {
            distance += Math.abs(v1[i] - v2[i]);
        }
        return distance;
    }
}
