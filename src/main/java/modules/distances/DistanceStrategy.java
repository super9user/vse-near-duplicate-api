package modules.distances;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Created by tanmay on 9/9/17.
 */
public abstract class DistanceStrategy {
    public double getDistance(double[] v1, double[] v2) throws Exception {
        throw new NotImplementedException();
    }
    public double getDistance(double[] v1, double[] v2, double sumV1, double sumV2) throws Exception {
        throw new NotImplementedException();
    }
}
