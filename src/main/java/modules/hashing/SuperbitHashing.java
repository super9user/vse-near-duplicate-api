package modules.hashing;

import config.HashingConstants;
import info.debatty.java.lsh.LSHSuperBit;

/**
 * Created by tanmay on 2/23/19.
 */

public class SuperbitHashing implements HashingStrategy {
    @Override
    public int[] getBuckets(double[] vector) throws Exception {
        int n = vector.length; // N dimensional
        LSHSuperBit lsh = new LSHSuperBit(HashingConstants.STAGES, HashingConstants.BUCKETS, n, HashingConstants.SEED);
        return lsh.hash(vector);
    }
}
