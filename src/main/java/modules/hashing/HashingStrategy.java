package modules.hashing;

/**
 * Created by tanmay on 9/9/17.
 */
public interface HashingStrategy {
    int[] getBuckets(double[] vector) throws Exception;
}
