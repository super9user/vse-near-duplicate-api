package modules.hashing;

import com.google.common.primitives.Doubles;
import config.HashingConstants;
import info.debatty.java.lsh.MinHash;
import java.util.Collections;
import java.util.List;

/**
 * Created by tanmay on 3/7/18.
 */
public class MinHashing implements HashingStrategy {

    @Override
    public int[] getBuckets(double[] vector) throws Exception {
        int n = vector.length; // N dimensional

        int signatureSize = computeSignatureSize(HashingConstants.STAGES);
        MinHash minHash = new MinHash(signatureSize, n, HashingConstants.SEED);
        long[][] hashCoefs = minHash.getCoefficients();
        int[] sig = getSignature(vector, hashCoefs, signatureSize, n);
        return hashSignature(sig, HashingConstants.STAGES, HashingConstants.BUCKETS);
    }

    private int[] getSignature(double[] vector, long[][] hashCoefs, int signatureSize, int n) {
        int[] sig = new int[signatureSize];
        for (int i = 0; i < signatureSize; i++) {
            sig[i] = Integer.MAX_VALUE;
        }
        List<Double> list = Doubles.asList(vector);
        Collections.sort(list);

        for (final double r : list) {
            for (int i = 0; i < signatureSize; i++) {
                sig[i] = Math.min( sig[i], h(i, r, hashCoefs, n));
            }
        }
        return sig;
    }

    private int h(final int i, final double x, long[][] hashCoefs, int dictSize) {
        return (int) ((hashCoefs[i][0] * (long) x + hashCoefs[i][1]) % dictSize);
    }

    private int computeSignatureSize(final int s) {
        return ((int) Math.ceil(Math.log(1.0 / s) / Math.log(HashingConstants.THRESHOLD)) + 1) * s;
    }

    /**
     * Hash a signature.
     * The signature is divided in s stages (or bands). Each stage is hashed to
     * one of the b buckets.
     * @param signature
     * @return An vector of s integers (between 0 and b-1)
     */
    public final int[] hashSignature(final int[] signature, int stages, int buckets) {

        // Create an accumulator for each stage
        int[] hash = new int[stages];

        // Number of rows per stage
        int rows = signature.length / stages;

        for (int i = 0; i < signature.length; i++) {
            int stage = Math.min(i / rows, stages - 1);
            hash[stage] = (int)
                    ((hash[stage] + (long) signature[i] * HashingConstants.LARGE_PRIME)
                            % buckets);

        }

        return hash;
    }
}
