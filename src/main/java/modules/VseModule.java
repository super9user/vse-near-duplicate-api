package modules;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import config.AppConstants;
import utils.AppUtils;

/**
 * Created by tanmay on 7/11/17.
 */
public abstract class VseModule {

    protected JsonArray compulsoryParams = new JsonArray();
    protected JsonObject optionalParams = new JsonObject();
    protected JsonObject params;
    protected AppUtils appUtils = AppUtils.getInstance();

    protected abstract JsonObject begin() throws Exception;
    protected abstract void setCompulsoryParams();
    protected abstract void setOptionalParamsWithDefaults();

    // combinator function
    public final JsonObject execute(JsonObject options) throws Exception {
        setCompulsoryParams();
        setOptionalParamsWithDefaults();
        this.params = appUtils.getParams(options, this.optionalParams);
        if(!appUtils.verifyOptions(this.params, this.compulsoryParams)) {
            JsonObject data = new JsonObject();
            data.addProperty("status", AppConstants.BAD_REQUEST_STATUS);
            return data;
        } else {
            return begin();
        }
    }
}
