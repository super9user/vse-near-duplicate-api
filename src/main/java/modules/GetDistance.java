package modules;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import config.AppConstants;
import modules.distances.DistanceStrategy;
import modules.distances.JaccardDistance;
import modules.distances.ManhattanDistance;
import modules.distances.TanimotoDistance;

/**
 * Created by tanmay on 9/9/17.
 */
public class GetDistance extends VseModule {
    JsonObject output;
    Gson gson;
    private DistanceStrategy distanceStrategyTanimoto;
    private DistanceStrategy distanceStrategyManhattan;
    private DistanceStrategy distanceStrategyJaccard;
    public GetDistance() {
        output = new JsonObject();
        gson = new Gson();
        distanceStrategyManhattan = new ManhattanDistance();
        distanceStrategyJaccard = new JaccardDistance();
        distanceStrategyTanimoto = new TanimotoDistance();
    }

    @Override
    protected JsonObject begin() throws Exception {
        double[] v1 = gson.fromJson(this.params.get("vector1"), double[].class);
        double[] v2 = gson.fromJson(this.params.get("vector2"), double[].class);
        DistanceStrategy distanceStrategy = null;

        switch (this.params.get("strategy").getAsString()) {
            case AppConstants.DISTANCE_STRATEGY_TANIMOTO:
                distanceStrategy = distanceStrategyTanimoto;
                break;

            case AppConstants.DISTANCE_STRATEGY_MANHATTAN:
                distanceStrategy = distanceStrategyManhattan;
                break;

            case AppConstants.DISTANCE_STRATEGY_JACCARD:
                distanceStrategy = distanceStrategyJaccard;
                break;

            default:
                throw new Exception("Strategy not found: " + this.params.get("strategy").getAsString());
        }
        if(this.params.get("v1Sum").isJsonNull() || this.params.get("v2Sum").isJsonNull()) {
            output.addProperty("distance", distanceStrategy.getDistance(v1, v2));
        } else {
            output.addProperty("distance", distanceStrategy.getDistance(v1, v2,
                    this.params.get("v1Sum").getAsDouble(), this.params.get("v2Sum").getAsDouble()));
        }
        return output;
    }

    @Override
    protected void setCompulsoryParams() {
        this.compulsoryParams.add("vector1");
        this.compulsoryParams.add("vector2");
    }

    @Override
    protected void setOptionalParamsWithDefaults() {
        this.optionalParams.addProperty("strategy", AppConstants.DISTANCE_STRATEGY_TANIMOTO);
        this.optionalParams.add("v1Sum", null);
        this.optionalParams.add("v2Sum", null);
    }
}
