package modules.averages;

import java.util.List;

/**
 * Created by tanmay on 9/9/17.
 */
public interface AverageStrategy {
    double getAverage(double[] v) throws Exception;
    double getAverage(List<Double> v) throws Exception;
}
