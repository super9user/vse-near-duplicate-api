package modules.averages;

import java.util.List;

/**
 * Created by tanmay on 10/23/17.
 */
public class MedianAverage implements AverageStrategy {
    @Override
    public double getAverage(double[] v) throws Exception {
        int middle = v.length/2;
        if (v.length % 2 == 1) {
            return v[middle];
        } else {
            return (v[middle-1] + v[middle]) / 2.0;
        }
    }

    @Override
    public double getAverage(List<Double> v) throws Exception {
        int middle = v.size()/2;
        if (v.size() % 2 == 1) {
            return v.get(middle);
        } else {
            return (v.get(middle - 1) + v.get(middle)) / 2.0;
        }
    }
}
