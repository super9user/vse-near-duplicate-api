package modules.averages;

import java.util.List;

/**
 * Created by tanmay on 10/23/17.
 */
public class MeanAverage implements AverageStrategy {
    @Override
    public double getAverage(double[] v) throws Exception {
        double sum = 0;
        for (int i = 0; i < v.length; i++) {
            sum += v[i];
        }
        return sum / v.length;
    }

    @Override
    public double getAverage(List<Double> v) throws Exception {
        double sum = 0;
        for(Double d: v) {
            sum += d;
        }
        return sum / v.size();
    }
}
