package modules;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import config.AppConstants;
import modules.averages.AverageStrategy;
import modules.averages.MeanAverage;
import modules.averages.MedianAverage;

/**
 * Created by tanmay on 9/9/17.
 */
public class GetAverage extends VseModule {
    JsonObject output;
    Gson gson;
    public GetAverage() {
        output = new JsonObject();
        gson = new Gson();
    }

    @Override
    protected JsonObject begin() throws Exception {
        double[] v = gson.fromJson(this.params.get("vector"), double[].class);
        AverageStrategy avgStrategy;

        switch (this.params.get("strategy").getAsString()) {
            case AppConstants.AVERAGE_STRATEGY_MEAN:
                avgStrategy = new MeanAverage();
                break;

            case AppConstants.AVERAGE_STRATEGY_MEDIAN:
                avgStrategy = new MedianAverage();
                break;

            default:
                throw new Exception("Strategy not found: " + this.params.get("strategy").getAsString());
        }

        output.addProperty("average", avgStrategy.getAverage(v));
        return output;
    }

    @Override
    protected void setCompulsoryParams() {
        this.compulsoryParams.add("vector");
    }

    @Override
    protected void setOptionalParamsWithDefaults() {
        this.optionalParams.addProperty("strategy", AppConstants.AVERAGE_STRATEGY_MEAN);
    }
}
