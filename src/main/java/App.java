/**
 * Created by tanmay on 6/21/17.
 */

import config.AppConstants;
import config.Environment;
import database.DatabaseOperations;
import database.Seeds;
import static spark.Spark.get;
import static spark.Spark.port;

public class App {


    public static void main(String[] args) {
        setEnvVariable(args);
        port(AppConstants.PORT);
        registerShutdownHook();

        System.out.println("App starting on PORT: " + AppConstants.PORT);
        System.out.println("Java ENV: " + Environment.JAVA_ENV);

        try {
            initiateDatabase();
            initiateRoutes();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static void registerShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            public void run() {
                DatabaseOperations dbOps = DatabaseOperations.getInstance();
                dbOps.close();
            }
        }));
    }

    private static void initiateDatabase() throws Exception {
        System.out.println("Initiating Database ...");
        Seeds dbSeeds = new Seeds();
        dbSeeds.connect();
        dbSeeds.createKeyspace();
        dbSeeds.createTypes();
        dbSeeds.createTables();
    }

    private static void initiateRoutes() {
        // Initiate all routes
        new routes.controllers.Index();
    }

    private static void setEnvVariable(String[] args) {
        if(args.length <= AppConstants.ARGS_ENV) return;
        String value = args[AppConstants.ARGS_ENV];
        if(value != null && (value == "prod" || value == "test")) Environment.JAVA_ENV = args[0];
    }
}
