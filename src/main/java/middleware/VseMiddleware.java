package middleware;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import config.AppConstants;
import routes.ResponseTemplate;
import utils.AppUtils;

import java.util.Map;

/**
 * Created by tanmay on 3/16/18.
 */
public abstract class VseMiddleware {

    protected JsonArray compulsoryParams = new JsonArray();
    protected JsonObject optionalParams = new JsonObject();
    protected JsonObject params;
    private AppUtils appUtils = AppUtils.getInstance();

    protected abstract JsonObject begin() throws Exception;
    protected abstract void setCompulsoryParams();
    protected abstract void setOptionalParamsWithDefaults();

    // combinator function
    // TODO - add authentication step
    public final ResponseTemplate execute(JsonObject options) {
        ResponseTemplate response = new ResponseTemplate();
        setCompulsoryParams();
        setOptionalParamsWithDefaults();
        this.params = appUtils.getParams(options, this.optionalParams);
        if(!appUtils.verifyOptions(this.params, this.compulsoryParams)) {
            response.setStatus(AppConstants.BAD_REQUEST_STATUS);
            response.setData("Please enter all compulsory parameters: " + this.compulsoryParams.getAsString());
        } else {
            JsonObject data = null;
            try{
                data = begin();
                if(data != null && data.has("status")) {
                    response.setStatus(data.get("status").getAsInt());
                } else {
                    response.setStatus(AppConstants.OK_STATUS);
                }
                response.setData(data);
            } catch (Exception e) {
                e.printStackTrace();
                response.setStatus(AppConstants.INTERNAL_SERVER_ERROR);
                response.setData("Error in " + this.getClass().getSimpleName() + ". " + e.toString());
            }
        }
        return response;
    }

}
