package middleware.search;

import com.datastax.driver.mapping.Result;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import config.AppConstants;
import database.models.image_by_bucket.ImageByBucketDAO;
import database.models.image_by_bucket.ImageByBucketModel;
import middleware.VseMiddleware;
import modules.GetDistance;
import modules.GetFeatureVector;
import modules.GetHashingBuckets;
import modules.distances.sort_distances.ImageDistance;
import modules.distances.sort_distances.ImageDistanceComparator;
import utils.FileUtils;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

/**
 * Created by tanmay on 3/7/18.
 */
public class HashingSearcher extends VseMiddleware {

    FileUtils fileUtils;
    ImageByBucketDAO imageByBucketDAO;
    Gson gson;
    GetFeatureVector getFeatureVector;
    JsonObject output;
    GetDistance getDistance;
    GetHashingBuckets getHashingBuckets;

    public HashingSearcher() {
        fileUtils = FileUtils.getInstance();
        gson = new Gson();
        getFeatureVector = new GetFeatureVector();
        output = new JsonObject();
        getDistance = new GetDistance();
        getHashingBuckets = new GetHashingBuckets();
        imageByBucketDAO = new ImageByBucketDAO();
    }

    @Override
    protected JsonObject begin() throws Exception {
        File img = new File(this.params.get("inputImagePath").getAsString());
        if(fileUtils.isValidImageFile(img)) {
            try {
                JsonElement resp = search(img);
                output.add("results", resp);
            } catch (Exception e) {
                e.printStackTrace();
                output.addProperty("error", e.getMessage());
                output.addProperty("status", AppConstants.INTERNAL_SERVER_ERROR);
            }
        } else {
            output.addProperty("error", "input not a valid image.");
            output.addProperty("status", AppConstants.BAD_REQUEST_STATUS);
        }
        return output;
    }

    private JsonElement search(File img) throws Exception {
        List<ImageDistance> imageDistances = new ArrayList<>(); // TODO - perf improv. init list with size.
        Set<String> set = new HashSet<>();

        double[] vector = getFeature(img);
        double vectorSum = DoubleStream.of(vector).sum();
        int[] buckets = getBuckets(vector);
        for(int i = 0; i < buckets.length; i++) {
            // TODO - prepared statement. maybe even batch?
            Result<ImageByBucketModel> result = imageByBucketDAO.getBy(buckets[i], i);
            for(ImageByBucketModel resultImg: result) {
                String fullPath = resultImg.getFullPath();
                if(!set.contains(fullPath)) {
                    set.add(fullPath);
                    double[] compareVector = resultImg.getFeatureVector();
                    double compareVectorSum = DoubleStream.of(compareVector).sum();
                    ImageDistance d = new ImageDistance(resultImg.getFullPath(), getDistance(vector,
                            compareVector, vectorSum, compareVectorSum));
                    imageDistances.add(d);
                }
            }
        }

        Collections.sort(imageDistances, new ImageDistanceComparator());
        System.out.println("Result length: " + imageDistances.size());
        List<ImageDistance> top100 = imageDistances.stream().limit(100).collect(Collectors.toList());
        return gson.toJsonTree(top100);
    }

    private double[] getFeature(File img) throws Exception {
        JsonObject params = new JsonObject();
        params.add("img", gson.toJsonTree(img));
        JsonObject output = getFeatureVector.execute(params);
        return gson.fromJson(output.get("featureVector"), double[].class);
    }

    private int[] getBuckets(double[] vector) throws Exception {
        JsonObject params = new JsonObject();
        params.add("vector", gson.toJsonTree(vector));
        params.addProperty("strategy", this.params.get("strategy").getAsString());
        JsonObject output = getHashingBuckets.execute(params);
        return gson.fromJson(output.get("buckets"), int[].class);
    }

    private double getDistance(double[] v1, double[] v2, double v1Sum, double v2Sum) throws Exception {
        JsonObject opts = new JsonObject();
        opts.add("vector1", gson.toJsonTree(v1));
        opts.add("vector2", gson.toJsonTree(v2));
        opts.add("v1Sum", gson.toJsonTree(v1Sum));
        opts.add("v2Sum", gson.toJsonTree(v2Sum));
        opts.addProperty("strategy", AppConstants.DISTANCE_STRATEGY_TANIMOTO);
        JsonObject resp = getDistance.execute(opts);
        return gson.fromJson(resp.get("distance"), double.class);
    }

    @Override
    protected void setCompulsoryParams() {
        this.compulsoryParams.add("inputImagePath");
    }

    @Override
    protected void setOptionalParamsWithDefaults() {
        this.optionalParams.addProperty("strategy", AppConstants.HASHING_STRATEGY_MINHASH);
    }

}
