package middleware.search;

import com.datastax.driver.mapping.Result;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import config.AppConstants;
import database.models.fcth_image.FcthImageDAO;
import database.models.fcth_image.FcthImageModel;
import middleware.VseMiddleware;
import modules.GetDistance;
import modules.distances.sort_distances.ImageDistance;
import modules.distances.sort_distances.ImageDistanceComparator;
import modules.feature_vector.FCTHFeatureVector;
import utils.FileUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by tanmay on 9/5/17.
 */
public class SimpleSearcher extends VseMiddleware {

    FileUtils fileUtils;
    FcthImageDAO fcthImageDAO;
    Gson gson;
    FCTHFeatureVector fcthFeatureVector;
    JsonObject output;
    GetDistance getDistance;

    public SimpleSearcher() {
        fileUtils = FileUtils.getInstance();
        fcthImageDAO = new FcthImageDAO();
        gson = new Gson();
        fcthFeatureVector = new FCTHFeatureVector();
        output = new JsonObject();
        getDistance = new GetDistance();
    }

    @Override
    protected JsonObject begin() {
        File img = new File(this.params.get("inputImagePath").getAsString());
        if(fileUtils.isValidImageFile(img)) {
            try {
                JsonElement resp = search(img);
                output.add("results", resp);
            } catch (Exception e) {
                e.printStackTrace();
                output.addProperty("error", e.getMessage());
                output.addProperty("status", AppConstants.INTERNAL_SERVER_ERROR);
            }
        } else {
            output.addProperty("error", "input not a valid fcth_image");
            output.addProperty("status", AppConstants.BAD_REQUEST_STATUS);
        }
        return output;
    }

    private JsonElement search(File img) throws Exception {
        double[] vector = fcthFeatureVector.getFeatureVector(img);

        // TODO - perf improv. only fetch necessary fields of images model
        Result<FcthImageModel> result = fcthImageDAO.getAll();
        List<ImageDistance> imageDistances = new ArrayList<>(); // TODO - perf improv. init list with size.
        for(FcthImageModel resultImg: result) {
            try {
                ImageDistance d = new ImageDistance(resultImg.getFullPath(), getDistance(vector, resultImg.getFeatureVector()));
                imageDistances.add(d);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("ImageDistance error for FcthImageModel id: " + resultImg.getId());
            }
        }
        Collections.sort(imageDistances, new ImageDistanceComparator());
        return gson.toJsonTree(imageDistances);
    }

    private double getDistance(double[] v1, double[] v2) throws Exception {
        JsonObject opts = new JsonObject();
        opts.add("vector1", gson.toJsonTree(v1));
        opts.add("vector2", gson.toJsonTree(v2));
        JsonObject resp = getDistance.execute(opts);
        return gson.fromJson(resp.get("distance"), double.class);
    }

    @Override
    protected void setCompulsoryParams() {
        this.compulsoryParams.add("inputImagePath");
    }

    @Override
    protected void setOptionalParamsWithDefaults() {}

}
