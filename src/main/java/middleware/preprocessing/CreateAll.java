package middleware.preprocessing;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import database.models.fcth_image.FcthImageDAO;
import database.models.fcth_image.FcthImageModel;
import database.models.image_by_bucket.ImageByBucketDAO;
import database.models.image_by_bucket.ImageByBucketModel;
import middleware.VseMiddleware;
import modules.GetFeatureVector;
import modules.GetHashingBuckets;
import utils.FileUtils;
import java.io.File;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by tanmay on 3/16/18.
 */
public class CreateAll extends VseMiddleware {

    Gson gson;
    FileUtils fileUtils;
    FcthImageDAO fcthImageDAO;
    ImageByBucketDAO imageByBucketDAO;
    GetFeatureVector getFeatureVector;
    GetHashingBuckets getHashingBuckets;
    JsonObject output;

    public CreateAll() {
        gson = new Gson();
        fileUtils = FileUtils.getInstance();
        fcthImageDAO = new FcthImageDAO();
        imageByBucketDAO = new ImageByBucketDAO();
        getFeatureVector = new GetFeatureVector();
        getHashingBuckets = new GetHashingBuckets();
        output = new JsonObject();
    }

    @Override
    protected JsonObject begin() throws Exception {
        JsonArray imageDirs = this.params.getAsJsonArray("imageDirs");
        for(JsonElement el: imageDirs) {
            JsonObject dirObj = el.getAsJsonObject();
            String group = dirObj.get("imageGroup").getAsString();
            String path = dirObj.get("path").getAsString();
            JsonObject stats = startIndexing(group, path);
            output.add(group, stats);
        }
        return output;
    }

    private JsonObject startIndexing(String group, String path) {

        JsonObject resp = new JsonObject();
        Collection images = fileUtils.getFilesInDir(path);
        resp.addProperty("totalImages", images.size());
        JsonArray invalidImages = new JsonArray();

        Iterator<File> iterator = images.iterator();
        int counter = 0;
        while (iterator.hasNext()) {
            if(counter % 1000 == 0) {
                System.out.println("Preprocessing counter: " + counter);
            }
            counter++;
            File img = iterator.next();
            if(fileUtils.isValidImageFile(img)) {
                try {
                    double[] vector = getFeature(img);
                    FcthImageModel fcthImage = new FcthImageModel(group, img.getAbsolutePath(), vector);
                    fcthImageDAO.saveAsync(fcthImage); // TODO - need to roll this back if anything below errors out

                    int[] buckets = getBuckets(vector);
                    for(int i = 0; i < buckets.length; i++) {
                        ImageByBucketModel imageByBucket = new ImageByBucketModel(buckets[i], i, vector,
                                img.getAbsolutePath(), fcthImage.getId());
                        imageByBucketDAO.saveAsync(imageByBucket);
                    }
                } catch (Exception e) {
                    invalidImages.add(img.getAbsolutePath());
                    e.printStackTrace();
                }
            } else {
                invalidImages.add(img.getAbsolutePath());
            }
        }
        resp.addProperty("invalidImages", invalidImages.size());
        resp.add("invalidImagesPaths", invalidImages);
        return resp;
    }

    private double[] getFeature(File img) throws Exception {
        JsonObject params = new JsonObject();
        params.add("img", gson.toJsonTree(img));
        JsonObject output = getFeatureVector.execute(params);
        return gson.fromJson(output.get("featureVector"), double[].class);
    }

    private int[] getBuckets(double[] vector) throws Exception {
        JsonObject params = new JsonObject();
        params.add("vector", gson.toJsonTree(vector));
        JsonObject output = getHashingBuckets.execute(params);
        return gson.fromJson(output.get("buckets"), int[].class);
    }

    @Override
    protected void setCompulsoryParams() {
        this.compulsoryParams.add("imageDirs");
    }

    @Override
    protected void setOptionalParamsWithDefaults() {}
}
