package middleware.preprocessing;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import database.models.fcth_image.FcthImageDAO;
import database.models.fcth_image.FcthImageModel;
import database.models.image_by_bucket.ImageByBucketDAO;
import database.models.image_by_bucket.ImageByBucketModel;
import middleware.VseMiddleware;
import modules.GetHashingBuckets;
import java.util.List;

/**
 * Created by tanmay on 2/23/19.
 */

public class CreateBuckets extends VseMiddleware {
    FcthImageDAO fcthImageDAO;
    ImageByBucketDAO imageByBucketDAO;
    GetHashingBuckets getHashingBuckets;
    Gson gson;
    public CreateBuckets() {
        gson = new Gson();
        fcthImageDAO = new FcthImageDAO();
        imageByBucketDAO = new ImageByBucketDAO();
        getHashingBuckets = new GetHashingBuckets();
    }

    @Override
    protected JsonObject begin() throws Exception {
        List<FcthImageModel> images = fcthImageDAO.getAll().all();
        JsonObject output = new JsonObject();
        int counter = 0;
        for(FcthImageModel image: images) {
            if(counter % 1000 == 0) {
                System.out.println("Preprocessing counter: " + counter);
            }
            counter++;
            double[] vector = image.getFeatureVector();
            int[] buckets = getBuckets(vector);
            for(int i = 0; i < buckets.length; i++) {
                ImageByBucketModel imageByBucket = new ImageByBucketModel(buckets[i], i, vector,
                        image.getFullPath(), image.getId());
                imageByBucketDAO.saveAsync(imageByBucket);
            }
        }
        output.addProperty("CreateCount", images.size());
        return null;
    }

    private int[] getBuckets(double[] vector) throws Exception {
        JsonObject params = new JsonObject();
        params.add("vector", gson.toJsonTree(vector));
        JsonObject output = getHashingBuckets.execute(params);
        return gson.fromJson(output.get("buckets"), int[].class);
    }

    @Override
    protected void setCompulsoryParams() {
    }

    @Override
    protected void setOptionalParamsWithDefaults() {
    }
}