package middleware;

import com.datastax.driver.mapping.Result;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import config.HashingConstants;
import database.models.fcth_image.FcthImageDAO;
import database.models.fcth_image.FcthImageModel;
import modules.GetHashingBuckets;

/**
 * Created by tanmay on 3/16/18.
 */
public class SimilarityTest extends VseMiddleware {
    Gson gson = new Gson();

    public SimilarityTest() {
    }

    @Override
    protected JsonObject begin() throws Exception {
        FcthImageDAO fcthImageDAO = new FcthImageDAO();
        Result<FcthImageModel> result = fcthImageDAO.getAll();

        int[][] counts = new int[HashingConstants.STAGES][HashingConstants.BUCKETS];
        int counter = 0;
        for(FcthImageModel iter: result) {
            if(counter % 1000 == 0) {
                System.out.println("Preprocessing counter: " + counter);
            }
            counter++;
            double[] vector = iter.getFeatureVector();
            GetHashingBuckets getHashingBuckets = new GetHashingBuckets();
            JsonObject params = new JsonObject();
            params.add("vector", gson.toJsonTree(vector));
            JsonObject hashObj = getHashingBuckets.execute(params);
            int[] hash = gson.fromJson(hashObj.get("buckets"), int[].class);
            for (int i = 0; i < hash.length; i++) {
                counts[i][hash[i]]++;
            }
        }

        System.out.println("Number of elements per bucket at each stage:");
        for (int i = 0; i < HashingConstants.STAGES; i++) {
            print(counts[i]);
            System.out.print("\n");
        }
        return null;
    }

    void print(int[] array) {
        System.out.print("[");
        for (int v : array) {
            System.out.print("" + v + " ");
        }
        System.out.print("]");
    }

    @Override
    protected void setCompulsoryParams() {
    }

    @Override
    protected void setOptionalParamsWithDefaults() {
    }
}
