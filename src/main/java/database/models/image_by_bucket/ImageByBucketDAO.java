package database.models.image_by_bucket;

import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.datastax.driver.mapping.Result;
import database.DatabaseOperations;

/**
 * Created by tanmay on 9/3/17.
 */
public class ImageByBucketDAO {

    MappingManager manager = DatabaseOperations.getInstance().getMappingManager();
    Mapper<ImageByBucketModel> mapper;
    ImageByBucketAccessor accessor;

    public ImageByBucketDAO() {
        mapper = manager.mapper(ImageByBucketModel.class);
        accessor = manager.createAccessor(ImageByBucketAccessor.class);
    }

    public void save(ImageByBucketModel image) {
        mapper.save(image);
    }

    public void saveAsync(ImageByBucketModel image) {
        mapper.saveAsync(image);
    }

    public Result<ImageByBucketModel> getAll() {
        return accessor.getAll();
    }

    public Result<ImageByBucketModel> getBy(int bucket, int stage) {
        return accessor.getBy(bucket, stage);
    }

    public ImageByBucketModel getOne() {
        Result<ImageByBucketModel> results = accessor.getOne();
        return results.one();
    }

    public int getCountAll() {
        return (int) accessor.getCountAll().one().getLong(0);
    }

}
