package database.models.image_by_bucket;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import config.CBIRConstants;
import config.DatabaseConstants;

import java.util.UUID;
import java.util.stream.DoubleStream;

/**
 * Created by tanmay on 8/19/17.
 */

@Table(keyspace = DatabaseConstants.KEYSPACE_NAME, name = CBIRConstants.BUCKET_TABLE_NAME)
public class ImageByBucketModel {

    @PartitionKey(0)
    private int bucket;
    @PartitionKey(1)
    private int stage;
    private UUID id;
    private UUID videoId;
    private double[] featureVector;
    private String fullPath;
    private Double featureVectorSum;

    public ImageByBucketModel() {} // Needed (as per java bean convention) by datastax drivers

    public ImageByBucketModel(int bucket, int stage, double[] vector, String fullPath, UUID videoId) {
        this.bucket = bucket;
        this.stage = stage;
        this.videoId = videoId;
        this.fullPath = fullPath;
        this.featureVector = vector;
        this.id = UUID.randomUUID();
        this.featureVectorSum = DoubleStream.of(vector).sum();
    }


    public double[] getFeatureVector() {
        return featureVector;
    }

    public void setFeatureVector(double[] featureVector) {
        this.featureVector = featureVector;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    public int getBucket() {
        return bucket;
    }

    public void setBucket(int bucket) {
        this.bucket = bucket;
    }

    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getVideoId() {
        return videoId;
    }

    public void setVideoId(UUID videoId) {
        this.videoId = videoId;
    }

    public Double getFeatureVectorSum() {
        return featureVectorSum;
    }

    public void setFeatureVectorSum(Double featureVectorSum) {
        this.featureVectorSum = featureVectorSum;
    }

}
