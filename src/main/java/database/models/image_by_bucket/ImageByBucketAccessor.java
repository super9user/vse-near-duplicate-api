package database.models.image_by_bucket;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Query;
import config.CBIRConstants;
import config.DatabaseConstants;

/**
 * Created by tanmay on 9/3/17.
 */
@Accessor
interface ImageByBucketAccessor {

    @Query("SELECT * FROM " + DatabaseConstants.KEYSPACE_NAME + "." + CBIRConstants.BUCKET_TABLE_NAME + ";")
    Result<ImageByBucketModel> getAll();

    @Query("SELECT * FROM " + DatabaseConstants.KEYSPACE_NAME + "." + CBIRConstants.BUCKET_TABLE_NAME + " where bucket=? AND stage=?;")
    Result<ImageByBucketModel> getBy(int bucket, int stage);

    @Query("SELECT * FROM " + DatabaseConstants.KEYSPACE_NAME + "." + CBIRConstants.BUCKET_TABLE_NAME + " limit 1;")
    Result<ImageByBucketModel> getOne();

    @Query("SELECT COUNT(*) FROM " + DatabaseConstants.KEYSPACE_NAME + "."+ CBIRConstants.BUCKET_TABLE_NAME + ";")
    ResultSet getCountAll();

}
