package database.models.image_by_bucket;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import config.CBIRConstants;
import config.DatabaseConstants;
import database.DatabaseOperations;

/**
 * Created by tanmay on 9/17/17.
 */
public class ImageByBucketTable {

    public static void createTable() throws Exception {
        String keyspace = DatabaseConstants.KEYSPACE_NAME;
        DatabaseOperations dataOps = DatabaseOperations.getInstance();
        JsonObject fields = new JsonObject();
        fields.addProperty("id", "uuid");
        fields.addProperty("videoId", "uuid");
        fields.addProperty("bucket", "int");
        fields.addProperty("stage", "int");
        fields.addProperty("fullPath", "text");
        fields.addProperty("featureVectorSum", "double");
        fields.addProperty("featureVector", "list<double>");

        JsonArray partitionKeys = new JsonArray();
        partitionKeys.add("bucket");
        partitionKeys.add("stage");
        JsonArray clusteringKeys = new JsonArray();
        clusteringKeys.add("videoId");
        dataOps.createTable(keyspace, CBIRConstants.BUCKET_TABLE_NAME, fields, partitionKeys, clusteringKeys);
        dataOps.verifyTableExists(keyspace, CBIRConstants.BUCKET_TABLE_NAME, fields);
    }

}
