package database.models.fcth_image;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import config.CBIRConstants;
import config.DatabaseConstants;
import database.DatabaseOperations;

/**
 * Created by tanmay on 9/17/17.
 */
public class FcthImageTable {

    public static void createTable() throws Exception {
        String keyspace = DatabaseConstants.KEYSPACE_NAME;
        DatabaseOperations dataOps = DatabaseOperations.getInstance();
        JsonObject fields = new JsonObject();
        fields.addProperty("id", "uuid");
        fields.addProperty("fullPath", "text");
        fields.addProperty("imageGroup", "text");
        fields.addProperty("featureVector", "list<double>");

        JsonArray partitionKeys = new JsonArray();
        partitionKeys.add("id");
        JsonArray clusteringKeys = new JsonArray();
        // TODO - add clustering key based on needs
        dataOps.createTable(keyspace, CBIRConstants.FCTH_TABLE_NAME, fields, partitionKeys, clusteringKeys);
        dataOps.verifyTableExists(keyspace, CBIRConstants.FCTH_TABLE_NAME, fields);
    }

}
