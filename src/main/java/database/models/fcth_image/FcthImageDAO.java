package database.models.fcth_image;

import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.datastax.driver.mapping.Result;
import database.DatabaseOperations;
import java.util.UUID;

/**
 * Created by tanmay on 9/3/17.
 */
public class FcthImageDAO {

    MappingManager manager = DatabaseOperations.getInstance().getMappingManager();
    Mapper<FcthImageModel> mapper;
    FcthImageAccessor accessor;

    public FcthImageDAO() {
        mapper = manager.mapper(FcthImageModel.class);
        accessor = manager.createAccessor(FcthImageAccessor.class);
    }

    public void save(FcthImageModel image) {
        mapper.save(image);
    }

    public void saveAsync(FcthImageModel image) {
        mapper.saveAsync(image);
    }

    public Result<FcthImageModel> getAll() {
        return accessor.getAll();
    }

    public Result<FcthImageModel> getById(UUID id) {
        return accessor.getById(id);
    }

    public FcthImageModel getOne() {
        Result<FcthImageModel> results = accessor.getOne();
        return results.one();
    }

    public int getCountAll() {
        return (int) accessor.getCountAll().one().getLong(0);
    }

}
