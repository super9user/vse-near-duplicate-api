package database.models.fcth_image;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import config.CBIRConstants;
import config.DatabaseConstants;
import java.util.UUID;

/**
 * Created by tanmay on 8/19/17.
 */

@Table(keyspace = DatabaseConstants.KEYSPACE_NAME, name = CBIRConstants.FCTH_TABLE_NAME)
public class FcthImageModel {

    @PartitionKey
    private UUID id;
    private String imageGroup;
    private double[] featureVector;
    private String fullPath;

    public FcthImageModel() {} // Needed (as per java bean convention) by datastax drivers

    public FcthImageModel(String imageGroup, String fullPath, double[] featureVector) {
        this.imageGroup = imageGroup;
        this.fullPath = fullPath;
        this.featureVector = featureVector;
        this.id = UUID.randomUUID();
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    public String getImageGroup() {
        return imageGroup;
    }

    public void setImageGroup(String imageGroup) {
        this.imageGroup = imageGroup;
    }

    public double[] getFeatureVector() {
        return featureVector;
    }

    public void setFeatureVector(double[] featureVector) {
        this.featureVector = featureVector;
    }

    public UUID getId() {
        return id;
    }

}
