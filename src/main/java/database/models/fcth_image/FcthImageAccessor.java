package database.models.fcth_image;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Query;
import config.CBIRConstants;
import config.DatabaseConstants;
import java.util.UUID;

/**
 * Created by tanmay on 9/3/17.
 */
@Accessor
interface FcthImageAccessor {

    @Query("SELECT * FROM " + DatabaseConstants.KEYSPACE_NAME + "." + CBIRConstants.FCTH_TABLE_NAME + ";")
    Result<FcthImageModel> getAll();

    @Query("SELECT * FROM " + DatabaseConstants.KEYSPACE_NAME + "." + CBIRConstants.FCTH_TABLE_NAME + " where id=?;")
    Result<FcthImageModel> getById(UUID id);

    @Query("SELECT * FROM " + DatabaseConstants.KEYSPACE_NAME + "." + CBIRConstants.FCTH_TABLE_NAME + " limit 1;")
    Result<FcthImageModel> getOne();

    @Query("SELECT COUNT(*) FROM " + DatabaseConstants.KEYSPACE_NAME + "."+ CBIRConstants.FCTH_TABLE_NAME + ";")
    ResultSet getCountAll();

}
