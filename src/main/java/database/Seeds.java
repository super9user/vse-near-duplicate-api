package database;

import com.google.gson.JsonObject;
import config.DatabaseConstants;
import database.models.fcth_image.FcthImageTable;
import database.models.image_by_bucket.ImageByBucketTable;
import utils.AppUtils;

/**
 * Created by tanmay on 8/19/17.
 */
public class Seeds {

    private JsonObject databaseDetails;
    private DatabaseOperations dbOps;
    private AppUtils appUtils;

    public Seeds() {
        databaseDetails = DatabaseConstants.DATABASE_DETAILS();
        dbOps = DatabaseOperations.getInstance();
        appUtils = AppUtils.getInstance();
    }

    public void connect() throws Exception {
        dbOps.connect(appUtils.jsonToStringArray(databaseDetails.get("contactPoints").getAsJsonArray()),
                databaseDetails.get("port").getAsInt(),
                databaseDetails.get("credentials").getAsJsonObject());
    }

    public void createKeyspace() throws Exception {
        String keyspaceName = DatabaseConstants.KEYSPACE_NAME;
        dbOps.createKeyspace(keyspaceName);
        dbOps.verifyKeySpace(keyspaceName);
    }

    public void createTypes() throws Exception {
        // Nothing yet
    }

    public void createTables() throws Exception {
        FcthImageTable.createTable();
        ImageByBucketTable.createTable();
    }

}
