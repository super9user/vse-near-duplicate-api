package database;

import com.datastax.driver.core.*;
import com.datastax.driver.extras.codecs.arrays.DoubleArrayCodec;
import com.datastax.driver.mapping.MappingManager;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import config.DatabaseConstants;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by tanmay on 8/19/17.
 */
public class DatabaseOperations {

    private Cluster cluster;
    private Session session;
    private static DatabaseOperations instance;
    private MappingManager mappingManager;

    // Singleton baby
    private DatabaseOperations() {}

    public static DatabaseOperations getInstance() {
        if( instance == null ) {
            instance = new DatabaseOperations();
        }
        return instance;
    }

    void connect(String[] contactPoints, Integer port, JsonObject credentials) throws Exception {
        cluster = Cluster.builder()
                .addContactPoints(contactPoints)
                .withCredentials(credentials.get("uname").getAsString(), credentials.get("pwd").getAsString())
                .withPort(port)
                .build();
        printMetaData();
        useCodecs();

        this.session = cluster.connect();
        if(this.session == null) throw new Exception("Could not connect to cassandra. Session object is empty");
        this.mappingManager = new MappingManager(this.session);
    }

    private void useCodecs() {
        cluster.getConfiguration().getCodecRegistry()
                .register(new DoubleArrayCodec());
    }

    private void printMetaData() {
        Metadata metadata = cluster.getMetadata();
        System.out.printf("Connected to cluster: %s\n", metadata.getClusterName());
        for ( Host host : metadata.getAllHosts() ) {
            System.out.printf("DataCenter: %s; Host: %s; Rack: %s\n", host.getDatacenter(), host.getAddress(), host.getRack());
        }
    }

    public Session getSession() {
        return this.session;
    }

    public MappingManager getMappingManager() {
        return this.mappingManager;
    }

    public void close() {
        System.out.println("Closing Cassandra connection ...");
        if(session != null) session.close();
        if(cluster != null) cluster.close();
    }

    void createKeyspace(String keyspaceName) throws Exception {
        createKeyspace(keyspaceName,
                DatabaseConstants.DEFAULT_REPLICATION_STRATEGY,
                DatabaseConstants.DEFAULT_REPLICATION_FACTOR);
    }

    void createKeyspace(String keyspaceName, String replicationStrategy, Integer replicationFactor) throws Exception {
        if(keyspaceName == null || replicationStrategy == null || replicationFactor == null)
            throw new Exception("Arguments cannot be null");

        StringBuilder sb = new StringBuilder("CREATE KEYSPACE IF NOT EXISTS ")
                .append(keyspaceName).append(" WITH replication = {")
                .append("'class':'").append(replicationStrategy)
                .append("','replication_factor':").append(replicationFactor)
                .append("};");
        String query = sb.toString();
        this.session.execute(query);
    }

    void verifyKeySpace(String keyspaceName) throws Exception {
        if(keyspaceName == null) throw new Exception("keyspaceName cannot be null");
        ResultSet result = session.execute("SELECT * FROM system_schema.keyspaces;");
        List<String> matchedKeyspaces = result.all()
                .stream()
                .filter(r -> r.getString(0).equals(keyspaceName.toLowerCase()))
                .map(r -> r.getString(0))
                .collect(Collectors.toList());

        if(matchedKeyspaces.size() != 1 || !matchedKeyspaces.get(0).equalsIgnoreCase(keyspaceName)) {
            throw new Exception("KeySpace " + keyspaceName + "was not created");
        }
    }

    public void createTable(String keyspace, String tableName, JsonObject fields, JsonArray partitionKeys,
                            JsonArray clusteringKeys) throws Exception {
        if(tableName == null || fields == null || partitionKeys == null || keyspace == null
                || fields.size() == 0 || partitionKeys.size() == 0)
            throw new Exception("Arguments cannot be null or empty");

        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ")
                .append(keyspace).append(".").append(tableName);

        sb.append("("); // bracket 1 start
        // For Fields
        for (Map.Entry<String,JsonElement> entry : fields.entrySet()) {
            sb.append(entry.getKey()).append(" ")
                    .append(entry.getValue().getAsString())
                    .append(",");
        }

        // For primary keys
        int counter = 0;
        sb.append("PRIMARY KEY ("); // bracket 2 start

        sb.append("("); // bracket 3 for partitionKeys start
        for (JsonElement param: partitionKeys) {
            sb.append(param.getAsString());
            counter++;
            if(counter < partitionKeys.size()) sb.append(", ");
        }
        sb.append(")"); // bracket 3 end

        counter = 0;
        if(clusteringKeys.size() > 0) sb.append(", ");
        for (JsonElement param: clusteringKeys) {
            sb.append(param.getAsString());
            counter++;
            if(counter < clusteringKeys.size()) sb.append(", ");
        }

        sb.append(")"); // bracket 2 end
        sb.append(")"); // bracket 1 end

        String query = sb.toString();
        session.execute(query);
    }

    public void createType(String keyspace, String typeName, JsonObject fields) throws Exception {
        if(typeName == null || fields == null || keyspace == null || fields.size() == 0 )
            throw new Exception("Arguments cannot be null or empty");

        StringBuilder sb = new StringBuilder("CREATE TYPE IF NOT EXISTS ")
                .append(keyspace).append(".").append(typeName);

        sb.append("(");
        // For Fields
        for (Map.Entry<String,JsonElement> entry : fields.entrySet()) {
            sb.append(entry.getKey()).append(" ")
                    .append(entry.getValue().getAsString())
                    .append(",");
        }
        sb.append(")");

        String query = sb.toString();
        session.execute(query);
    }

    public void verifyTableExists(String keyspace, String tableName, JsonObject fields) throws Exception {
        ResultSet result = session.execute(
                "SELECT * FROM " + keyspace + "." + tableName + ";");

        List<String> columnNames =
                result.getColumnDefinitions().asList().stream()
                        .map(cl -> cl.getName())
                        .collect(Collectors.toList());

        if(columnNames.size() != fields.size()) {
            throw new Exception("Table " + tableName + " was not created properly");
        }
    }

}
